function a01_calculate_beta_power(id, rootpath)

% check mu-beta dynamics as an indicator of motor preparation

if ismac % run if function is not pre-compiled
    id = '1126'; % test for example subject
    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..'))
    rootpath = pwd;
end

pn.dataIn	= fullfile(rootpath, '..', 'X1_preprocEEGData');
pn.out      = fullfile(rootpath, 'data', 'K_motor');
pn.behav    = fullfile(rootpath, '..', '..', 'stsw_beh_task', 'behavior', 'data');
pn.tools	= fullfile(rootpath, 'tools'); addpath(pn.tools);
addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;

display(['processing ID ' id]);

%% load data

tmp = [];
tmp.clock = tic; % set tic for processing duration

load(fullfile(pn.dataIn, [id, '_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat']), 'data');

%% save TrlInfo from FT

TrlInfo = data.TrlInfo;
TrlInfoLabels = data.TrlInfoLabels;

%% CSD transform

csd_cfg = [];
csd_cfg.elecfile = 'standard_1005.elc';
csd_cfg.method = 'spline';
data = ft_scalpcurrentdensity(csd_cfg, data);

%% wavelet transform (7 cycles): alpha-beta motor

cfg                 = [];
cfg.channel         = 'all';
cfg.method          = 'wavelet';
cfg.width           = 7;
cfg.keeptrials      = 'yes';
cfg.output          = 'pow';
cfg.foi             = 8:1:25;
cfg.toi             = 2:0.025:9.5; % 40 Hz
TFRdata_AlphaBeta   = ft_freqanalysis(cfg, data);

%% re-segment data with respect to probe onset & response 

% probe onset

timepoint = repmat(6.04, size(TFRdata_AlphaBeta.powspctrm,1),1);
time = repmat(TFRdata_AlphaBeta.time, size(TFRdata_AlphaBeta.powspctrm,1),1);
[~, idx] = min(abs(timepoint-time), [],2);
cfg = [];
cfg.begsample = idx-3*40; % now 40 Hz
cfg.endsample = idx+40;
dataProbe_AlphaBeta = TFRdata_AlphaBeta;
dataProbe_AlphaBeta.powspctrm = NaN(size(data.trial,2),60,numel(TFRdata_AlphaBeta.freq), cfg.endsample(1)-(cfg.begsample(1)-1));
for indTrial = 1:size(data.trial,2)
    dataProbe_AlphaBeta.powspctrm(indTrial,:,:,:) = TFRdata_AlphaBeta.powspctrm(indTrial,:,:,cfg.begsample(indTrial):cfg.endsample(indTrial));
end
dataProbe_AlphaBeta.time = -3*40*25:25:40*25;

% response

timepoint = repmat(6.04+TrlInfo(:,9), 1,size(TFRdata_AlphaBeta.powspctrm,4));   
time = repmat(TFRdata_AlphaBeta.time, size(TFRdata_AlphaBeta.powspctrm,1),1);
[~, idx] = min(abs(timepoint-time), [],2);
cfg = [];
cfg.begsample = idx-3*40; % now 40 Hz
cfg.endsample = idx+40;
trialToRemove = find(cfg.begsample<=0 | cfg.endsample>size(TFRdata_AlphaBeta.powspctrm,4));
dataResponse_AlphaBeta = TFRdata_AlphaBeta;
dataResponse_AlphaBeta.powspctrm = NaN(size(data.trial,2),60,numel(TFRdata_AlphaBeta.freq), cfg.endsample(1)-(cfg.begsample(1)-1));
for indTrial = 1:size(data.trial,2)
    if ismember(indTrial, trialToRemove)
        dataResponse_AlphaBeta.powspctrm(indTrial,:,:,:) = NaN;
    else
        dataResponse_AlphaBeta.powspctrm(indTrial,:,:,:) = TFRdata_AlphaBeta.powspctrm(indTrial,:,:,cfg.begsample(indTrial):cfg.endsample(indTrial));
    end
end
dataResponse_AlphaBeta.time = -3*40*25:25:40*25; % in ms

% remove empty trials from all structures

dataProbe_AlphaBeta.powspctrm(trialToRemove,:,:,:) = []; dataProbe_AlphaBeta.trialToRemove = numel(trialToRemove);
dataResponse_AlphaBeta.powspctrm(trialToRemove,:,:,:) = []; dataResponse_AlphaBeta.trialToRemove = numel(trialToRemove);
TrlInfo(trialToRemove,:) = [];
disp(['Removed trials: ', num2str(numel(trialToRemove))]);

%% split into left vs. right response & condition
% do this for both TFR and time domain data

% identify left/right response for each trial
behavData = load(fullfile(pn.behav, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'));
leftRightVector = behavData.MergedDataEEG.TargetOption(TrlInfo(:,7),... % 1 = left; 2 = right
    find(strcmp(behavData.IDs_all, id)));

Accs = behavData.MergedDataEEG.Accs(TrlInfo(:,7),find(strcmp(behavData.IDs_all, id)));

% determine which response was given
for indTrial = 1:numel(leftRightVector)
    if leftRightVector(indTrial) == 1 & Accs(indTrial) == 1 % correct resp was left, correct resp, resp = left
        leftRightVector(indTrial) = 1;
    elseif leftRightVector(indTrial) == 1 & Accs(indTrial) == 0 % correct resp was left, incorrect resp, resp = right
        leftRightVector(indTrial) = 2;
    elseif leftRightVector(indTrial) == 2 & Accs(indTrial) == 1 % correct resp was right, correct resp, resp = right
        leftRightVector(indTrial) = 2;
    elseif leftRightVector(indTrial) == 2 & Accs(indTrial) == 0 % correct resp was right, incorrect resp, resp = left
        leftRightVector(indTrial) = 1;
    end
end

% flip left and right channels in layout

%     chanLocs = cellfun(@(x) x(end), data.label,'UniformOutput',false);
%     chanLocs(strcmp(chanLocs, {'z'})) = {'NaN'};
%     chanLocs = cellfun(@str2num,chanLocs,'un',0);
%     chanLocs = cell2mat(chanLocs);
% 
%     odd = mod(chanLocs,2) == 1; % left EEG channels
%     even = mod(chanLocs,2) == 0; % right EEG channels

labels_odd = {'Fp1';'AF7';'AF3';'F7';'F5';'F3';'F1';'FT7';'FC5';'FC3';'FC1';'T7';'C5';'C3';'C1';'TP7';'CP5';'CP3';'CP1';'P7';'P5';'P3';'PO9';'PO7';'PO3';'O1'};    
labels_even = {'Fp2';'AF8';'AF4';'F8';'F6';'F4';'F2';'FT8';'FC6';'FC4';'FC2';'T8';'C6';'C4';'C2';'TP8';'CP6';'CP4';'CP2';'P8';'P6';'P4';'PO10';'PO8';'PO4';'O2'};

odd = find(ismember(data.label, labels_odd));
even = find(ismember(data.label, labels_even));

%[labels_odd, labels_even] % check that labels are corresponding!

% for left responses, switch left and right channels, i.e. left is ALWAYS contralateral
% left response: left channels represent right channels (contralateral response)
% left response: right channels represent left channels (ipsilateral response)
% right response: original layout (i.e., contralateral = left)

curBackup = dataProbe_AlphaBeta.powspctrm;
dataProbe_AlphaBeta.powspctrm(leftRightVector == 1,odd,:) = curBackup(leftRightVector == 1,even,:);
dataProbe_AlphaBeta.powspctrm(leftRightVector == 1,even,:) = curBackup(leftRightVector == 1,odd,:);

curBackup = dataResponse_AlphaBeta.powspctrm;
dataResponse_AlphaBeta.powspctrm(leftRightVector == 1,odd,:) = curBackup(leftRightVector == 1,even,:);
dataResponse_AlphaBeta.powspctrm(leftRightVector == 1,even,:) = curBackup(leftRightVector == 1,odd,:);

%% calculate single-trial baseline (at -400:-100 ms preprobe)

idxBL = dataProbe_AlphaBeta.time > -400 & dataProbe_AlphaBeta.time < -100;
numTime = size(dataProbe_AlphaBeta.powspctrm,4);
bl = repmat(squeeze(nanmean(dataProbe_AlphaBeta.powspctrm(:,:,:,idxBL),4)),1,1,1,numTime);

dataProbe_AlphaBeta_bl = dataProbe_AlphaBeta;
dataProbe_AlphaBeta_bl.powspctrm = (dataProbe_AlphaBeta.powspctrm-bl)./bl;

dataResponse_AlphaBeta_bl = dataResponse_AlphaBeta;
dataResponse_AlphaBeta_bl.powspctrm = (dataResponse_AlphaBeta.powspctrm-bl)./bl;

% average within condition

for indCond = 1:4
    cfg = [];
    cfg.trials = TrlInfo(:,8)==indCond;
    % timelock to probe onset
    dataProbeAvg_AlphaBeta{indCond} = dataProbe_AlphaBeta; % Hack for data including multiple freqs, which get concatenated by FT
    dataProbeAvg_AlphaBeta{indCond}.powspctrm = ...
        squeeze(nanmean(dataProbeAvg_AlphaBeta{indCond}.powspctrm(cfg.trials,:,:,:),1));
    dataProbeAvg_AlphaBeta{indCond}.dimord = 'chan_freq_time';
    % timelock to response
    dataResponseAvg_AlphaBeta{indCond} = dataResponse_AlphaBeta;
    dataResponseAvg_AlphaBeta{indCond}.powspctrm = ...
        squeeze(nanmean(dataResponseAvg_AlphaBeta{indCond}.powspctrm(cfg.trials,:,:,:),1));
    dataResponseAvg_AlphaBeta{indCond}.dimord = 'chan_freq_time';
    % diff versions
    dataProbeAvg_AlphaBeta_bl{indCond} = dataProbe_AlphaBeta_bl; % Hack for data including multiple freqs, which get concatenated by FT
    dataProbeAvg_AlphaBeta_bl{indCond}.powspctrm = ...
        squeeze(nanmean(dataProbeAvg_AlphaBeta_bl{indCond}.powspctrm(cfg.trials,:,:,:),1));
    dataProbeAvg_AlphaBeta_bl{indCond}.dimord = 'chan_freq_time';

    dataResponseAvg_AlphaBeta_bl{indCond} = dataResponse_AlphaBeta_bl;
    dataResponseAvg_AlphaBeta_bl{indCond}.powspctrm = ...
        squeeze(nanmean(dataResponseAvg_AlphaBeta_bl{indCond}.powspctrm(cfg.trials,:,:,:),1));
    dataResponseAvg_AlphaBeta_bl{indCond}.dimord = 'chan_freq_time';
end

%% save computed files

save(fullfile(pn.out, [id, '_motor.mat']), ...
    'dataProbeAvg_AlphaBeta',...
    'dataResponseAvg_AlphaBeta',...
	'dataProbeAvg_AlphaBeta_bl',...
    'dataResponseAvg_AlphaBeta_bl');

clear data*
    
end