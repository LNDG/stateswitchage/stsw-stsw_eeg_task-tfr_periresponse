% check mu-beta dynamics as an indicator of motor preparation

%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.History      = [pn.root, 'A_preproc/SA_preproc_study/B_data/D_History/'];
pn.tools        = [pn.root, 'B_analyses/S2_TFR_PeriResponse/T_tools/']; addpath(pn.tools);
pn.out          = [pn.root, 'B_analyses/S2_TFR_PeriResponse/B_data/K_motor/'];

addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

%% define Condition & IDs for preprocessing

condEEG = 'dynamic';

%% define IDs for segmentation

% N = 47 YAs + 53 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

for id = 1:length(IDs)
    display(['processing ID ' IDs{id}]);

    %% load data

    tmp = [];
    tmp.clock = tic; % set tic for processing duration

    load([pn.History, IDs{id}, '_', condEEG, '_config.mat'],'config');
    load([pn.dataIn, IDs{id}, '_', condEEG, '_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat'], 'data');

    %% save TrlInfo from FT
    
    TrlInfo = data.TrlInfo;
    TrlInfoLabels = data.TrlInfoLabels;
    
    %% CSD transform
    
    csd_cfg = [];
    csd_cfg.elecfile = 'standard_1005.elc';
    csd_cfg.method = 'spline';
    data = ft_scalpcurrentdensity(csd_cfg, data);
    
    %% wavelet transform (7 cycles): alpha-beta motor

    cfg                 = [];
    cfg.channel         = 'all';
    cfg.method          = 'wavelet';
    cfg.width           = 7;
    cfg.keeptrials      = 'yes';
    cfg.output          = 'pow';
    cfg.foi             = 8:1:25;
    cfg.toi             = -1:0.05:9.5; % 20 Hz
    TFRdata_AlphaBeta   = ft_freqanalysis(cfg, data);
    
    %% re-segment data with respect to probe onset & response 
    
    % stim onset
    timepoint = repmat(3, size(TFRdata_AlphaBeta.powspctrm,1),1);
    time = repmat(TFRdata_AlphaBeta.time, size(TFRdata_AlphaBeta.powspctrm,1),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-20; % now 20 Hz
    cfg.endsample = idx+120;
    dataStim_AlphaBeta = TFRdata_AlphaBeta;
    dataStim_AlphaBeta.powspctrm = NaN(size(data.trial,2),60,numel(TFRdata_AlphaBeta.freq), cfg.endsample(1)-(cfg.begsample(1)-1));
    for indTrial = 1:size(data.trial,2)
        dataStim_AlphaBeta.powspctrm(indTrial,:,:,:) = TFRdata_AlphaBeta.powspctrm(indTrial,:,:,cfg.begsample(indTrial):cfg.endsample(indTrial));
    end
    dataStim_AlphaBeta.time = -20*50:50:120*50;
   
    % probe onset
    
    timepoint = repmat(6.04, size(data.trial,2),1);
    time = repmat(data.time{1}, size(data.trial,2),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-400;
    cfg.endsample = idx+400;
    dataProbe = data;
    dataProbe.trial = NaN(size(data.trial,2),60,cfg.endsample(1)-(cfg.begsample(1)-1));
    for indTrial = 1:size(data.trial,2)
        dataProbe.trial(indTrial,:,:) = data.trial{indTrial}(:,cfg.begsample(indTrial):cfg.endsample(indTrial));
    end
    dataProbe.time = -400*2:2:400*2;
    
    timepoint = repmat(6.04, size(TFRdata_AlphaBeta.powspctrm,1),1);
    time = repmat(TFRdata_AlphaBeta.time, size(TFRdata_AlphaBeta.powspctrm,1),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-100; % now 20 Hz
    cfg.endsample = idx+40;
    dataProbe_AlphaBeta = TFRdata_AlphaBeta;
    dataProbe_AlphaBeta.powspctrm = NaN(size(data.trial,2),60,numel(TFRdata_AlphaBeta.freq), cfg.endsample(1)-(cfg.begsample(1)-1));
    for indTrial = 1:size(data.trial,2)
        dataProbe_AlphaBeta.powspctrm(indTrial,:,:,:) = TFRdata_AlphaBeta.powspctrm(indTrial,:,:,cfg.begsample(indTrial):cfg.endsample(indTrial));
    end
    dataProbe_AlphaBeta.time = -100*50:50:40*50;
    
    % response
    
    timepoint = repmat(6.04+TrlInfo(:,9), 1,size(data.time{1},2));
    time = repmat(data.time{1}, size(data.trial,2),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-400;
    cfg.endsample = idx+400;
    trialToRemove = find(cfg.begsample<=0 | cfg.endsample>size(data.trial{1},2));
    dataResponse = data;
    dataResponse.trial = NaN(size(data.trial,2),60,cfg.endsample(1)-(cfg.begsample(1)-1));
    for indTrial = 1:size(data.trial,2)
        if ismember(indTrial, trialToRemove)
            dataResponse.trial(indTrial,:,:) = NaN;
        else
        	dataResponse.trial(indTrial,:,:) = data.trial{indTrial}(:,cfg.begsample(indTrial):cfg.endsample(indTrial));
        end
    end
    dataResponse.time = -400*2:2:400*2;
    
    timepoint = repmat(6.04+TrlInfo(:,9), 1,size(TFRdata_AlphaBeta.powspctrm,4));   
    time = repmat(TFRdata_AlphaBeta.time, size(TFRdata_AlphaBeta.powspctrm,1),1);
    [~, idx] = min(abs(timepoint-time), [],2);
    cfg = [];
    cfg.begsample = idx-40; % now 20 Hz
    cfg.endsample = idx+20;
    trialToRemove = [trialToRemove; find(cfg.begsample<=0 | cfg.endsample>size(TFRdata_AlphaBeta.powspctrm,4))];
    dataResponse_AlphaBeta = TFRdata_AlphaBeta;
    dataResponse_AlphaBeta.powspctrm = NaN(size(data.trial,2),60,numel(TFRdata_AlphaBeta.freq), cfg.endsample(1)-(cfg.begsample(1)-1));
    for indTrial = 1:size(data.trial,2)
        if ismember(indTrial, trialToRemove)
            dataResponse_AlphaBeta.powspctrm(indTrial,:,:,:) = NaN;
        else
            dataResponse_AlphaBeta.powspctrm(indTrial,:,:,:) = TFRdata_AlphaBeta.powspctrm(indTrial,:,:,cfg.begsample(indTrial):cfg.endsample(indTrial));
        end
    end
    dataResponse_AlphaBeta.time = [-40*.05:.05:20*.05].*1000; % in ms
    
    % remove empty trials from all sturctures
    
    dataProbe.trial(trialToRemove,:,:) = []; dataProbe.trialToRemove = numel(trialToRemove);
    dataProbe_AlphaBeta.powspctrm(trialToRemove,:,:,:) = []; dataProbe_AlphaBeta.trialToRemove = numel(trialToRemove);
    
    dataResponse.trial(trialToRemove,:,:) = []; dataResponse.trialToRemove = numel(trialToRemove);
    dataResponse_AlphaBeta.powspctrm(trialToRemove,:,:,:) = []; dataResponse_AlphaBeta.trialToRemove = numel(trialToRemove);
    
    TrlInfo(trialToRemove,:) = [];
    
    disp(['Removed trials: ', num2str(numel(trialToRemove))]);
    
    %% split into left vs. right response & condition
    % do this for both TFR and time domain data
    
    % identify left/right response for each trial
    behavData = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat');
    leftRightVector = behavData.MergedDataEEG.TargetOption(TrlInfo(:,7),... % 1 = left; 2 = right
        find(strcmp(behavData.IDs_all, IDs{id})));
    
    Accs = behavData.MergedDataEEG.Accs(TrlInfo(:,7),find(strcmp(behavData.IDs_all, IDs{id})));
    
    % Define trials based on the executed motor response (not necessarily
    % the correct choice).
    
    % determine which response was given
    for indTrial = 1:numel(leftRightVector)
        if leftRightVector(indTrial) == 1 & Accs(indTrial) == 1 % correct resp was left, correct resp, resp = left
            leftRightVector(indTrial) = 1;
        elseif leftRightVector(indTrial) == 1 & Accs(indTrial) == 0 % correct resp was left, incorrect resp, resp = right
            leftRightVector(indTrial) = 2;
        elseif leftRightVector(indTrial) == 2 & Accs(indTrial) == 1 % correct resp was right, correct resp, resp = right
            leftRightVector(indTrial) = 2;
        elseif leftRightVector(indTrial) == 2 & Accs(indTrial) == 0 % correct resp was right, incorrect resp, resp = left
            leftRightVector(indTrial) = 1;
        end
    end
    
    % flip left and right channels in layout
    
%     chanLocs = cellfun(@(x) x(end), data.label,'UniformOutput',false);
%     chanLocs(strcmp(chanLocs, {'z'})) = {'NaN'};
%     chanLocs = cellfun(@str2num,chanLocs,'un',0);
%     chanLocs = cell2mat(chanLocs);
% 
%     odd = mod(chanLocs,2) == 1; % left EEG channels
%     even = mod(chanLocs,2) == 0; % right EEG channels
    
    % odd channels are located on the left, even channels are located on the right
    labels_odd = {'Fp1';'AF7';'AF3';'F7';'F5';'F3';'F1';'FT7';'FC5';'FC3';'FC1';'T7';'C5';'C3';'C1';'TP7';'CP5';'CP3';'CP1';'P7';'P5';'P3';'PO9';'PO7';'PO3';'O1'};    
    labels_even = {'Fp2';'AF8';'AF4';'F8';'F6';'F4';'F2';'FT8';'FC6';'FC4';'FC2';'T8';'C6';'C4';'C2';'TP8';'CP6';'CP4';'CP2';'P8';'P6';'P4';'PO10';'PO8';'PO4';'O2'};
    
    odd = find(ismember(data.label, labels_odd));
    even = find(ismember(data.label, labels_even));
    
    %[labels_odd, labels_even] % check that labels are corresponding!
    
    % for left responses, switch left and right channels, i.e. left is
    % ALWAYS contralateral (odd channels after realignment)
    % left response: left channels represent right channels (contralateral response)
    % left response: right channels represent left channels (ipsilateral response)
    % right response: original layout (i.e., contralateral = left)
    
    curBackup = dataStim_AlphaBeta.powspctrm;
    dataStim_AlphaBeta.powspctrm(leftRightVector == 1,odd,:) = curBackup(leftRightVector == 1,even,:);
    dataStim_AlphaBeta.powspctrm(leftRightVector == 1,even,:) = curBackup(leftRightVector == 1,odd,:);
    
    curBackup = dataProbe.trial;
    dataProbe.trial(leftRightVector == 1,odd,:) = curBackup(leftRightVector == 1,even,:);
    dataProbe.trial(leftRightVector == 1,even,:) = curBackup(leftRightVector == 1,odd,:);

    curBackup = dataProbe_AlphaBeta.powspctrm;
    dataProbe_AlphaBeta.powspctrm(leftRightVector == 1,odd,:) = curBackup(leftRightVector == 1,even,:);
    dataProbe_AlphaBeta.powspctrm(leftRightVector == 1,even,:) = curBackup(leftRightVector == 1,odd,:);
    
    curBackup = dataResponse.trial;
    dataResponse.trial(leftRightVector == 1,odd,:) = curBackup(leftRightVector == 1,even,:);
    dataResponse.trial(leftRightVector == 1,even,:) = curBackup(leftRightVector == 1,odd,:);
    
    curBackup = dataResponse_AlphaBeta.powspctrm;
    dataResponse_AlphaBeta.powspctrm(leftRightVector == 1,odd,:) = curBackup(leftRightVector == 1,even,:);
    dataResponse_AlphaBeta.powspctrm(leftRightVector == 1,even,:) = curBackup(leftRightVector == 1,odd,:);
    
    % calculate single-trial power difference left vs. right
    % left channels represent contra-ipsi; right channels represent ipsi-contra 
    
    dataStim_AlphaBetaDiff = dataStim_AlphaBeta;
    dataStim_AlphaBetaDiff.powspctrm(:,even,:) = dataStim_AlphaBeta.powspctrm(:,even,:) - dataStim_AlphaBeta.powspctrm(:,odd,:);
    dataStim_AlphaBetaDiff.powspctrm(:,odd,:) = dataStim_AlphaBeta.powspctrm(:,odd,:) - dataStim_AlphaBeta.powspctrm(:,even,:);
    
    dataProbeDiff = dataProbe;
    dataProbeDiff.trial(:,even,:) = dataProbe.trial(:,even,:) - dataProbe.trial(:,odd,:);
    dataProbeDiff.trial(:,odd,:) = dataProbe.trial(:,odd,:) - dataProbe.trial(:,even,:);
    
    dataProbe_AlphaBetaDiff = dataProbe_AlphaBeta;
    dataProbe_AlphaBetaDiff.powspctrm(:,even,:) = dataProbe_AlphaBeta.powspctrm(:,even,:) - dataProbe_AlphaBeta.powspctrm(:,odd,:);
    dataProbe_AlphaBetaDiff.powspctrm(:,odd,:) = dataProbe_AlphaBeta.powspctrm(:,odd,:) - dataProbe_AlphaBeta.powspctrm(:,even,:);
    
    dataResponseDiff = dataResponse;
    dataResponseDiff.trial(:,even,:) = dataResponse.trial(:,even,:) - dataResponse.trial(:,odd,:);
    dataResponseDiff.trial(:,odd,:) = dataResponse.trial(:,odd,:) - dataResponse.trial(:,even,:);
    
    dataResponse_AlphaBetaDiff = dataResponse_AlphaBeta;
    dataResponse_AlphaBetaDiff.powspctrm(:,even,:) = dataResponse_AlphaBeta.powspctrm(:,even,:) - dataResponse_AlphaBeta.powspctrm(:,odd,:);
    dataResponse_AlphaBetaDiff.powspctrm(:,odd,:) = dataResponse_AlphaBeta.powspctrm(:,odd,:) - dataResponse_AlphaBeta.powspctrm(:,even,:);
    
    %% instead of averaging within condition, show 1 target, and reponses with target convergence
    
    % add target convergence for each trial in TrlInfo structure
    
    behavData = load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'], 'MergedDataEEG', 'MergedDataMRI', 'IDs_all');
    
    curField = ['MergedDataEEG'];
    
    idx_ID = find(strcmp(behavData.IDs_all, IDs{id}));
    
    UniqueResponses = NaN(256,1);

    count = 1;
    for indRun = 1:4 % loop across runs
        for indBlock = 1:8 % loop across blocks
            for indTrial = 1:8 % loop across trials
                cuedAttributes = behavData.(curField).expInfo{idx_ID}.AttCuesRun{indRun}{indBlock,indTrial};
                cuedResponses = behavData.(curField).expInfo{idx_ID}.HighProbChoiceRun{indRun}{indBlock,indTrial}(cuedAttributes);
                targetResponse = behavData.(curField).expInfo{idx_ID}.targetOptionRun{indRun}(indBlock,indTrial);
                UniqueResponses(count,1) = sum(ismember(cuedResponses, targetResponse))/numel(cuedResponses);
                count = count + 1;
            end
        end
    end
    
    % restrict to trials in the EEG data
    
    UniqueResponses = UniqueResponses(TrlInfo(:,7));
    
    % average within condition
    
    for indCond = 1:3
        cfg = [];
        if indCond == 1 % only trials with single target
            cfg.trials = TrlInfo(:,8)==1;
        elseif indCond == 2 % only trials with multiple targets where responses CORRESPOND across features
            cfg.trials = find(TrlInfo(:,8) > 1 & UniqueResponses==1);
        elseif indCond == 3 % only trials with multiple targets where responses DO NOT CORRESPOND across features
            cfg.trials = find(TrlInfo(:,8) > 1 & UniqueResponses~=1);
            % both of the above
            %cfg.trials = [find(TrlInfo(:,8) > 1 & UniqueResponses==1); find(TrlInfo(:,8)==1)];
        end
        dataStimAvg_AlphaBeta{indCond} = dataStim_AlphaBeta; % Hack for data including multiple freqs, which get concatenated by FT
        dataStimAvg_AlphaBeta{indCond}.powspctrm = ...
            squeeze(nanmean(dataStimAvg_AlphaBeta{indCond}.powspctrm(cfg.trials,:,:,:),1));
        dataStimAvg_AlphaBeta{indCond}.dimord = 'chan_freq_time';
        % timelock to probe onset
        dataProbeAvg{indCond} = ft_timelockanalysis(cfg, dataProbe);
        dataProbeAvg_AlphaBeta{indCond} = dataProbe_AlphaBeta; % Hack for data including multiple freqs, which get concatenated by FT
        dataProbeAvg_AlphaBeta{indCond}.powspctrm = ...
            squeeze(nanmean(dataProbeAvg_AlphaBeta{indCond}.powspctrm(cfg.trials,:,:,:),1));
        dataProbeAvg_AlphaBeta{indCond}.dimord = 'chan_freq_time';
        % timelock to response
        dataResponseAvg{indCond} = ft_timelockanalysis(cfg, dataResponse);
        dataResponseAvg_AlphaBeta{indCond} = dataResponse_AlphaBeta;
        dataResponseAvg_AlphaBeta{indCond}.powspctrm = ...
            squeeze(nanmean(dataResponseAvg_AlphaBeta{indCond}.powspctrm(cfg.trials,:,:,:),1));
        dataResponseAvg_AlphaBeta{indCond}.dimord = 'chan_freq_time';
        % diff versions
        dataStimAvg_AlphaBetaDiff{indCond} = dataStim_AlphaBetaDiff; % Hack for data including multiple freqs, which get concatenated by FT
        dataStimAvg_AlphaBetaDiff{indCond}.powspctrm = ...
            squeeze(nanmean(dataStimAvg_AlphaBetaDiff{indCond}.powspctrm(cfg.trials,:,:,:),1));
        dataStimAvg_AlphaBetaDiff{indCond}.dimord = 'chan_freq_time';
        dataProbeAvgDiff{indCond} = ft_timelockanalysis(cfg, dataProbeDiff);
        dataProbeAvg_AlphaBetaDiff{indCond} = dataProbe_AlphaBetaDiff; % Hack for data including multiple freqs, which get concatenated by FT
        dataProbeAvg_AlphaBetaDiff{indCond}.powspctrm = ...
            squeeze(nanmean(dataProbeAvg_AlphaBetaDiff{indCond}.powspctrm(cfg.trials,:,:,:),1));
        dataProbeAvg_AlphaBetaDiff{indCond}.dimord = 'chan_freq_time';
        dataResponseAvgDiff{indCond} = ft_timelockanalysis(cfg, dataResponseDiff);
        dataResponseAvg_AlphaBetaDiff{indCond} = dataResponse_AlphaBetaDiff;
        dataResponseAvg_AlphaBetaDiff{indCond}.powspctrm = ...
            squeeze(nanmean(dataResponseAvg_AlphaBetaDiff{indCond}.powspctrm(cfg.trials,:,:,:),1));
        dataResponseAvg_AlphaBetaDiff{indCond}.dimord = 'chan_freq_time';
    end
    
    %% save computed files

    save([pn.out, IDs{id}, '_motor_byCorr_v1.mat'], 'dataStimAvg_AlphaBeta','dataProbeAvg', 'dataProbeAvg_AlphaBeta',...
        'dataResponseAvg', 'dataResponseAvg_AlphaBeta',...
        'dataStimAvg_AlphaBetaDiff','dataProbeAvgDiff', 'dataProbeAvg_AlphaBetaDiff',...
        'dataResponseAvgDiff', 'dataResponseAvg_AlphaBetaDiff');

    clear data*
    
end % ID
