% check mu-beta dynamics as an indicator of motor preparation

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.tools        = fullfile(pn.root, 'tools'); addpath(pn.tools);
pn.figures      = fullfile(pn.root, 'figures');
pn.out          = fullfile(pn.root, 'data', 'K_motor');

addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
addpath(fullfile(pn.tools, 'BrewerMap'));

%% define IDs 

filename = fullfile(pn.root, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

%% define conditions

fields = {'dataProbeAvg_AlphaBeta',...
    'dataResponseAvg_AlphaBeta',...
	'dataProbeAvg_AlphaBeta_bl',...
    'dataResponseAvg_AlphaBeta_bl'};

%% collect all subjects

ERPstruct = [];
for id = 1:length(IDs)
    tmp = load(fullfile(pn.out, [IDs{id}, '_motor.mat']));
    for indField = 1:numel(fields)
        tfr{indField}(id, :, :) = squeeze(nanmean(tmp.(fields{indField}){1}.powspctrm([26, 27, 35, 36],:,:),1));
        for indCond = 1:4
            if isfield(tmp.(fields{indField}){indCond}, 'powspctrm')
                % average across alpha-beta freqs
                cfg = [];
                cfg.avgoverfreq = 'yes';
                cfg.frequency = [12, 25];
                tmp.(fields{indField}){indCond} = ft_selectdata(cfg, tmp.(fields{indField}){indCond});
                tmp.(fields{indField}){indCond}.avg = squeeze(tmp.(fields{indField}){indCond}.powspctrm);
                tmp.(fields{indField}){indCond} = rmfield(tmp.(fields{indField}){indCond}, 'powspctrm');
                tmp.(fields{indField}){indCond}.dimord = 'chan_time';
            end
            ERPstruct.(fields{indField})(:,id) = tmp.(fields{indField});
        end
    end
end

clear tmp;

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

for indField = 1:numel(fields)
    for indAge = 1:2
        for indCond = 1:4
            curIdx = find(ageIdx{indAge});
            OUT.(fields{indField}){indCond,indAge} = ERPstruct.(fields{indField}){1,1};
            for indID = 1:numel(curIdx)
                OUT.(fields{indField}){indCond,indAge}.individual(indID,:,:) = ERPstruct.(fields{indField}){indCond,curIdx(indID)}.avg;
            end
        end
    end
end

save(fullfile(pn.out, 'motor_merged.mat'), '-struct', 'OUT')

h = figure;
for indField = 1:numel(fields)
    subplot(1,numel(fields),indField);
    imagesc(squeeze(nanmean(tfr{indField}(:, :, :),1)))
end
figureName = ['a02_tfr'];
saveas(h, fullfile(pn.figures, figureName), 'png');