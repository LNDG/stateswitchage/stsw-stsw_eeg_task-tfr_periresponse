clc; close all; clear all;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

addpath(fullfile(pn.root, 'code'));

a02b_plot_beta('YA')
a02b_plot_beta('OA')

disp('Step finished!')

close all; clc;