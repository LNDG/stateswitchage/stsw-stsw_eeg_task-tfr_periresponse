function a02b_plot_beta(age)

% check mu-beta dynamics as an indicator of motor preparation

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

pn.out          = fullfile(pn.root, 'data', 'K_motor');
pn.tools        = fullfile(pn.root, 'tools'); addpath(pn.tools);
pn.figures      = fullfile(pn.root, 'figures');

addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
addpath(fullfile(pn.tools, 'BrewerMap'));
addpath(fullfile(pn.tools, 'shadedErrorBar'));
addpath(genpath(fullfile(pn.tools, 'RainCloudPlots')));

%% define IDs 

filename = fullfile(pn.root, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

if strcmp(age, 'YA')
    indAge = 1;
elseif strcmp(age, 'OA')
    indAge = 2;
end

%% collect all subjects

load(fullfile(pn.out, 'motor_merged.mat'), ...
    'dataResponseAvg_AlphaBeta', 'dataProbeAvg_AlphaBeta')

%% baseline correction: performed on grand average beta series (condition-specific!)
% always normalize by pre-probe values

beta_data = dataResponseAvg_AlphaBeta;
bl_data = dataProbeAvg_AlphaBeta;

data_for_analysis = [];
idxBL = bl_data{1,1}.time >-400 & bl_data{1,1}.time<-200; % use time indices of probe
for indCond = 1:4
    numTime = size(beta_data{indCond,indAge}.individual,3);
    bl = repmat(squeeze(nanmean(bl_data{indCond,indAge}.individual(:,:,idxBL),3)),1,1,numTime);
    data_for_analysis{indCond,indAge} = (beta_data{indCond,indAge}.individual-bl)./bl;
end

%% Figure: plot overview of contralateral beta decreases

cBrew = brewermap(6,'Blues');
cBrew = cBrew(3:6,:);
            
h = figure('units','normalized','position',[.1 .1 .3 .4]);
set(0, 'DefaultFigureRenderer', 'painters');

% between-subject error bars
% rearrange data in matrix
for indCond = 1:4
    BLBeta(indCond,:,:) = squeeze(nanmean(data_for_analysis{indCond,indAge}(:,[26, 27, 35, 36],:),2));
end
cla; hold on;
% between-subject error bars
% new value = old value ? subject average + grand average
condAvg = squeeze(nanmean(BLBeta,1));
curData = squeeze(BLBeta(1,:,:));
curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l1 = shadedErrorBar(beta_data{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .1);
SourceData_1M = nanmean(curData,1); SourceData_1SE = standError; SourceData_time = beta_data{1,1}.time;
curData = squeeze(BLBeta(2,:,:));
curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l2 = shadedErrorBar(beta_data{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(2,:),'linewidth', 2}, 'patchSaturation', .1);
SourceData_2M = nanmean(curData,1); SourceData_2SE = standError;
curData = squeeze(BLBeta(3,:,:));
curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l3 = shadedErrorBar(beta_data{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(3,:),'linewidth', 2}, 'patchSaturation', .1);
SourceData_3M = nanmean(curData,1); SourceData_3SE = standError;
curData = squeeze(BLBeta(4,:,:));
curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
standError = nanstd(curData,1)./sqrt(size(curData,1));
l4 = shadedErrorBar(beta_data{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .1);
SourceData_4M = nanmean(curData,1); SourceData_4SE = standError;
xlim([-1500 200]); xlabel('Time (ms); resp-locked')
line([0 0], get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2)
legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine], {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'}, 'location', 'NorthEast')
legend('boxoff');
ylabel({'Normalized contralateral mu-beta';'(12-25 Hz) power (a.u.)'});
title({'Lateralized beta slope indicates drift rate decreases';''})

% fit regression lines from -200 to -50 prior to response (cf. McGovern et al.)
    
dataForSlopeFit = []; slopes = [];
for indCond = 1:4
    curTime = beta_data{1,1}.time;
    dataForSlopeFit{indAge}(:,indCond,:) = squeeze(nanmean(data_for_analysis{indCond,indAge}(:,[26, 27, 35, 36],curTime>-200 & curTime<-50),2));
    for indID = 1:size(dataForSlopeFit{indAge},1)
        slopes{indAge}(indID,indCond,:) = polyfit(1:size(dataForSlopeFit{indAge},3),squeeze(dataForSlopeFit{indAge}(indID,indCond,:))',1);

    end
end
    
    
% Inset 1: plot bar with significance indication for lateralized beta

%     condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
%     condPairsLevel = [7*10^-8 -17*10^-8 9*10^-8 11*10^-8 16*10^-8 20*10^-8];
condPairs = [1,2];
condPairsLevel = [7*10^-8];

ax2 = axes('Position',[.2 .2 .2 .23]);
box off;
hold on;

data = []; data_ws = [];
for i = 1:4
    for j = 1:1
        data{i, j} = squeeze(slopes{indAge}(:,i,1));
        % individually demean for within-subject visualization
        data_ws{i, j} = slopes{indAge}(:,i,1)-...
            nanmean(slopes{indAge}(:,:,1),2)+...
            repmat(nanmean(nanmean(slopes{indAge}(:,:,1),2),1),size(slopes{indAge}(:,:,1),1),1);
    end
end

% IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

cl = cBrew(4,:);
cla;
    h_rc = rm_raincloud(data_ws, cl,1);
    % add stats
    %condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
    %condPairsLevel = [7.5*10^-6 9*10^-6 12*10^-6 12*10^-6 5*10^-6 17*10^-6]+13*10^-6;
    condPairs = [1,2];
    condPairsLevel = [9*10^-7];
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
           mysigstar_vert(gca, [condPairsLevel(indPair), condPairsLevel(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

        end
    end
    view([90 -90]);
    axis ij
box(gca,'off')
%set(gca, 'YTick', [1,2,3,4]);
set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
ylabel('Target load'); xlabel({'CPP Slope'})
set(findall(gcf,'-property','FontSize'),'FontSize',20)
%xlim([-10 10]*10^-7)
yticks = get(gca, 'ytick'); ylim([yticks(1)-(yticks(2)-yticks(1))./1.5, yticks(4)+(yticks(2)-yticks(1))./1.5]);
curData = [data{1, 1}, data{2, 1}, data{3, 1}, data{4, 1}];
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
[~, p, ci, stats] = ttest(IndividualSlopes);
title(['linear: p = ', num2str(round(p,3))]); 

set(findall(gcf,'-property','FontSize'),'FontSize',25)
set(findall(ax2,'-property','FontSize'),'FontSize',20)

figureName = ['A_contralateralBeta_',age];

saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% save beta slope values

BetaSlope.data = squeeze(slopes{indAge}(:,:,1));
BetaSlope.IDs = IDs(ageIdx{indAge});

save(fullfile(pn.root, 'data', ['BetaSlope_',age,'.mat']), 'BetaSlope');

%% extract source data

SourceData_1M = SourceData_1M(SourceData_time >= -1500 & SourceData_time<= 200); 
SourceData_1SE = SourceData_1SE(SourceData_time >= -1500 & SourceData_time<= 200);
SourceData_2M = SourceData_2M(SourceData_time >= -1500 & SourceData_time<= 200); 
SourceData_2SE = SourceData_2SE(SourceData_time >= -1500 & SourceData_time<= 200);
SourceData_3M = SourceData_3M(SourceData_time >= -1500 & SourceData_time<= 200); 
SourceData_3SE = SourceData_3SE(SourceData_time >= -1500 & SourceData_time<= 200);
SourceData_4M = SourceData_4M(SourceData_time >= -1500 & SourceData_time<= 200); 
SourceData_4SE = SourceData_4SE(SourceData_time >= -1500 & SourceData_time<= 200);
SourceData_time = SourceData_time(SourceData_time >= -1500 & SourceData_time<= 200); 

SourceData = [SourceData_1M', SourceData_1SE', SourceData_2M', SourceData_2SE',...
    SourceData_3M', SourceData_3SE',SourceData_4M', SourceData_4SE',SourceData_time'];

SourceData_slopes = BetaSlope.data;

% figure; hold on;
% plot(SourceData_time, SourceData_1M)
% plot(SourceData_time, SourceData_2M)
% plot(SourceData_time, SourceData_3M)
% plot(SourceData_time, SourceData_4M)

%% Figure: plot topography of lateral beta around response

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'EastOutside';
%cfg.zlim = [-.4 .4];

h = figure('units','normalized','position',[.1 .1 .2 .2]);
plotData = [];
plotData.label = beta_data{1,1}.label; % {1 x N}
plotData.dimord = 'chan';

cfg.marker = 'off';
cfg.highlight = 'on';
cfg.highlightcolor = [1 1 1];
cfg.highlightsymbol = '.';
cfg.highlightsize = 20;
cfg.highlightchannel = beta_data{1,indAge}.label([26, 27, 35, 36]); % 31, 32, 40, 41
cfg.figure = h;

idxTime = beta_data{1,indAge}.time>-50 & beta_data{1,indAge}.time<50;
% average across loads
LoadAverageL = squeeze(nanmean(cat(4, data_for_analysis{1,indAge}, ...
                                        data_for_analysis{2,indAge}, ...
                                        data_for_analysis{3,indAge}, ...
                                        data_for_analysis{4,indAge}),4));
plotData.powspctrm = squeeze(nanmean(nanmean(LoadAverageL(:,:,idxTime),3),1))';
ft_topoplotER(cfg,plotData);

cb = colorbar(cfg.colorbar); set(get(cb,'label'),'string','Mu-beta power (a.u.)');
title({'Beta: -50:50 ms peri-response'; ''})

cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)

set(findall(gcf,'-property','FontSize'),'FontSize',20)

figureName = ['B_contralateralBeta_topography_',age];

saveas(h, fullfile(pn.figures, figureName), 'fig');
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% test for threshold differences at contralateral beta minimum

for indCond = 1:4
    curTime = beta_data{1,1}.time;
    dataForThreshold{indAge}(:,indCond) = squeeze(nanmean(nanmean(data_for_analysis...
        {indCond,indAge}(:,[26, 27, 35, 36],curTime>-50 & curTime <= 50),2),3));
end

%% plot threshold RCP

condPairs = [1,2; 2,3; 3,4];
condPairsLevel = [];
condPairsLevel{1} = [-2*10^-6 -2.3*10^-8 -2.6*10^-8 -2.9*10^-8 -3.2*10^-8 -3.5*10^-8];
lims{1} = [-8 3]*10^-6;
colorm = [.5,.5,.5];
paramLabels = {'Beta [-50:50 ms]'}; 

h = figure('units','normalized','position',[.1 .1 .15 .15]);
set(0, 'DefaultFigureRenderer', 'painters');    
cla; hold on;

dat = dataForThreshold{indAge};

% read into cell array of the appropriate dimensions
data = []; data_ws = [];
for i = 1:4
    for j = 1:1
        data{i, j} = squeeze(dat(:,i));
        % individually demean for within-subject visualization
        data_ws{i, j} = dat(:,i)-...
            nanmean(dat(:,:),2)+...
            repmat(nanmean(nanmean(dat(:,:),2),1),size(dat(:,:),1),1);
    end
end

% IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

cl = colorm(1,:);

box off
cla;
    h_rc = rm_raincloud(data_ws, cl,1);
    % add stats
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
           mysigstar_vert(gca, [condPairsLevel{1}(indPair), condPairsLevel{1}(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

        end
    end
    view([90 -90]);
    axis ij
box(gca,'off')
%set(gca, 'YTick', [1,2,3,4]);
set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
ylabel('Target load'); xlabel(paramLabels{1})
set(findall(gcf,'-property','FontSize'),'FontSize',20)
%xlim(lims{1}); 
curYTick = get(gca, 'YTick'); ylim([curYTick(1)-.5*(curYTick(2)-curYTick(1)) curYTick(4)+.5*(curYTick(2)-curYTick(1))]);

% check linear effect
X = [1 1; 1 2; 1 3; 1 4]; b=X\dat'; IndividualSlopes_load = b(2,:);
[~, p] = ttest(IndividualSlopes_load)
title(['linear p: ', num2str(round(p,3))])

set(findall(gcf,'-property','FontSize'),'FontSize',20)

figureName = ['C_contralateralBeta_threshold_',age];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% save beta threshold values

BetaThreshold.data = dataForThreshold{indAge};
BetaThreshold.IDs = IDs(ageIdx{indAge});

save(fullfile(pn.root, 'data', ['BetaThreshold_',age,'.mat']), 'BetaThreshold');

%% SourceData
        
SourceData = BetaThreshold.data;

end