% check mu-beta dynamics as an indicator of motor preparation

%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

pn.tools        = '/Volumes/LNDG/Programs_Tools_Scripts/Downloaded_programs&tools/'; addpath(pn.tools);
addpath(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/T_tools/fieldtrip-20170904/']); ft_defaults;
pn.out = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/B_data/K_motor/';
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')

%% define Condition & IDs for preprocessing

condEEG = 'dynamic';

%% define IDs for segmentation

% N = 47 YAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

%% collect all subjects

ERPstruct = [];
for id = 1:length(IDs)
    load([pn.out, IDs{id}, '_motor_byCorr_v1.mat'], 'dataStimAvg_AlphaBeta', 'dataStimAvg_AlphaBetaDiff', ...
        'dataProbeAvg', 'dataProbeAvg_AlphaBeta', 'dataProbeAvgDiff', 'dataProbeAvg_AlphaBetaDiff',...
        'dataResponseAvg', 'dataResponseAvg_AlphaBeta', 'dataResponseAvgDiff','dataResponseAvg_AlphaBetaDiff');
    ERPstruct.dataProbeAvg(:,id) = dataProbeAvg;
    ERPstruct.dataProbeAvgDiff(:,id) = dataProbeAvgDiff;
    freqs = dataProbeAvg_AlphaBeta{1,1}.freq > 8 & dataProbeAvg_AlphaBeta{1,1}.freq < 25;
    % average across alpha-beta freqs
    for indCond = 1:3
        dataStimAvg_AlphaBeta{indCond}.avg = squeeze(nanmean(dataStimAvg_AlphaBeta{indCond}.powspctrm(:,freqs,:),2));
        dataStimAvg_AlphaBeta{indCond}.dimord = 'chan_time';
        dataStimAvg_AlphaBeta{indCond} = ...
            rmfield(dataStimAvg_AlphaBeta{indCond}, 'powspctrm');
        dataStimAvg_AlphaBeta{indCond} = ...
            rmfield(dataStimAvg_AlphaBeta{indCond}, 'freq');
        
        dataProbeAvg_AlphaBeta{indCond}.avg = squeeze(nanmean(dataProbeAvg_AlphaBeta{indCond}.powspctrm(:,freqs,:),2));
        dataProbeAvg_AlphaBeta{indCond}.dimord = 'chan_time';
        dataProbeAvg_AlphaBeta{indCond} = ...
            rmfield(dataProbeAvg_AlphaBeta{indCond}, 'powspctrm');
        dataProbeAvg_AlphaBeta{indCond} = ...
            rmfield(dataProbeAvg_AlphaBeta{indCond}, 'freq');
        
        dataResponseAvg_AlphaBeta{indCond}.avg = squeeze(nanmean(dataResponseAvg_AlphaBeta{indCond}.powspctrm(:,freqs,:),2));
        dataResponseAvg_AlphaBeta{indCond}.dimord = 'chan_time';
        dataResponseAvg_AlphaBeta{indCond} = ...
            rmfield(dataResponseAvg_AlphaBeta{indCond}, 'powspctrm');
        dataResponseAvg_AlphaBeta{indCond} = ...
            rmfield(dataResponseAvg_AlphaBeta{indCond}, 'freq');
    end
    ERPstruct.dataStimAvg_AlphaBeta(:,id) = dataStimAvg_AlphaBeta;
    ERPstruct.dataProbeAvg_AlphaBeta(:,id) = dataProbeAvg_AlphaBeta;
    ERPstruct.dataResponseAvg(:,id) = dataResponseAvg;
    ERPstruct.dataResponseAvg_AlphaBeta(:,id) = dataResponseAvg_AlphaBeta;
end

clear dataProbeAvg_AlphaBeta dataProbeAvg_AlphaBetaDiff dataResponseAvg_AlphaBeta dataResponseAvg_AlphaBetaDiff dataStimAvg_AlphaBetaDiff;

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

cfg = [];
for indAge = 1
    for indCond = 1:3
        cfg.keepindividual = 'yes';
        dataStimAvg_AlphaBeta{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataStimAvg_AlphaBeta{indCond,ageIdx{indAge}});
        dataProbeAvg_AlphaBeta{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataProbeAvg_AlphaBeta{indCond,ageIdx{indAge}});
        dataResponseAvg_AlphaBeta{indCond,indAge} = ft_timelockgrandaverage(cfg, ERPstruct.dataResponseAvg_AlphaBeta{indCond,ageIdx{indAge}});
    end
end

save([pn.out, 'Merged_motor_byCorr_v1.mat'], ...
    'dataStimAvg_AlphaBeta', 'dataStimAvg_AlphaBetaDiff', ...
    'dataProbeAvg', 'dataProbeAvg_AlphaBeta', 'dataResponseAvg', 'dataResponseAvg_AlphaBeta', ...
    'dataProbeAvgDiff', 'dataProbeAvg_AlphaBetaDiff', 'dataResponseAvgDiff', 'dataResponseAvg_AlphaBetaDiff')

load([pn.out, 'Merged_motor_byCorr_v1.mat'], ...
    'dataStimAvg_AlphaBeta', 'dataStimAvg_AlphaBetaDiff', ...
    'dataProbeAvg', 'dataProbeAvg_AlphaBeta', 'dataResponseAvg', 'dataResponseAvg_AlphaBeta', ...
    'dataProbeAvgDiff', 'dataProbeAvg_AlphaBetaDiff', 'dataResponseAvgDiff', 'dataResponseAvg_AlphaBetaDiff')


%% CBPA: F-test between conditions

dataYA = [];
for indID = 1:47
    for indCond = 1:3
        dataYA{indCond, indID}.data = permute(squeeze(nanmean(dataStimAvg_AlphaBeta{indCond,1}.individual(indID,[26, 27, 35, 36],:),2)),[2,1]); % contralateral
        dataYA{indCond, indID}.dimord = 'chan_time';
        dataYA{indCond, indID}.time = dataStimAvg_AlphaBeta{indCond,1}.time;
        dataYA{indCond, indID}.label{1} = 'mubeta';
    end
    % add chance comparison condition
    dataYA{4, indID} = dataYA{1, indID};
    dataYA{4, indID}.data = repmat(0, size(dataYA{1, indID}.data,1), size(dataYA{1, indID}.data,2));
end

cfgStat = [];
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesFmultivariate'; % ft_statfun_depsamplesregrT
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 0;
cfgStat.tail             = 1;
cfgStat.clustertail      = 1;
cfgStat.alpha            = 0.05;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'data';
cfgStat.neighbours      = []; % no neighbors here 

subj = 47;
conds = 3;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_1] = ft_timelockstatistics(cfgStat, dataYA{1,:}, dataYA{2,:}, dataYA{3,:});

% perform individual t-tests

cfgStat.statistic        = 'ft_statfun_depsamplesT'; % ft_statfun_depsamplesT
subj = 47;
conds = 2;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_1_1] = ft_timelockstatistics(cfgStat, dataYA{1,:}, dataYA{3,:});
[stat_1_2] = ft_timelockstatistics(cfgStat, dataYA{2,:}, dataYA{3,:});


dataYA = [];
for indID = 1:47
    for indCond = 1:3
        dataYA{indCond, indID}.data = permute(squeeze(nanmean(dataStimAvg_AlphaBeta{indCond,1}.individual(indID,[30, 31, 39, 40],:),2)),[2,1]); % contralateral
        dataYA{indCond, indID}.dimord = 'chan_time';
        dataYA{indCond, indID}.time = dataStimAvg_AlphaBeta{indCond,1}.time;
        dataYA{indCond, indID}.label{1} = 'mubeta';
    end
    % add chance comparison condition
    dataYA{4, indID} = dataYA{1, indID};
    dataYA{4, indID}.data = repmat(0, size(dataYA{1, indID}.data,1), size(dataYA{1, indID}.data,2));
end

cfgStat = [];
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesFmultivariate'; % ft_statfun_depsamplesregrT
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 0;
cfgStat.tail             = 1;
cfgStat.clustertail      = 1;
cfgStat.alpha            = 0.05;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'data';
cfgStat.neighbours      = []; % no neighbors here 

subj = 47;
conds = 3;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_2] = ft_timelockstatistics(cfgStat, dataYA{1,:}, dataYA{2,:}, dataYA{3,:});

% perform individual t-tests

cfgStat.statistic        = 'ft_statfun_depsamplesT'; % ft_statfun_depsamplesT
subj = 47;
conds = 2;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_2_1] = ft_timelockstatistics(cfgStat, dataYA{1,:}, dataYA{3,:});
[stat_2_2] = ft_timelockstatistics(cfgStat, dataYA{2,:}, dataYA{3,:});

%% CBPA: contra-ipsi vs. 0

dataYA = [];
for indID = 1:47
    for indCond = 1:3
        dataYA{indCond, indID}.data = permute(squeeze(nanmean(dataStimAvg_AlphaBeta{indCond,1}.individual(indID,[26, 27, 35, 36],:)-...
            dataStimAvg_AlphaBeta{indCond,1}.individual(indID,[30, 31, 39, 40],:),2)),[2,1]); % contralateral
        dataYA{indCond, indID}.dimord = 'chan_time';
        dataYA{indCond, indID}.time = dataStimAvg_AlphaBeta{indCond,1}.time;
        dataYA{indCond, indID}.label{1} = 'mubeta';
    end
    % add chance comparison condition
    dataYA{4, indID} = dataYA{1, indID};
    dataYA{4, indID}.data = repmat(0, size(dataYA{1, indID}.data,1), size(dataYA{1, indID}.data,2));
end

cfgStat = [];
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesT'; % ft_statfun_depsamplesregrT
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 0;
cfgStat.tail             = 1;
cfgStat.clustertail      = 1;
cfgStat.alpha            = 0.05;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'data';
cfgStat.neighbours      = []; % no neighbors here 

subj = 47;
conds = 2;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_3] = ft_timelockstatistics(cfgStat, dataYA{1,:},dataYA{4,:});
[stat_4] = ft_timelockstatistics(cfgStat, dataYA{2,:},dataYA{4,:});
[stat_5] = ft_timelockstatistics(cfgStat, dataYA{3,:},dataYA{4,:});

%% Figure: plot overview of contralateral beta decreases (stimulus-locked)

% This highlights a drop in contralateral mu-beta starting after stimulus
% onset. But this is likely simply a representation of alpha desync that
% smeared into the beta range. One way to overcome this challenge may be to
% subtract ipsilateral beta. It would also be good to include
% non-convergent responses as a control condition.

dataStimAvg_AlphaBeta{1,1}.time = dataStimAvg_AlphaBeta{1,1}.time/1000;

% cBrew = brewermap(6,'Blues');
% cBrew = cBrew(3:6,:);
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(4,'RdBu');

pn.shadedError = ['/Volumes/Kosciessa/Tools/shadedErrorBar-7002ebc']; addpath(pn.shadedError);
            
    h = figure('units','normalized','position',[.1 .1 .3 .4]);
    set(0, 'DefaultFigureRenderer', 'painters');
    subplot(2,1,1);
    BLBeta = [];
    for indCond = 1:3
        BLBeta(indCond,:,:) = squeeze(nanmean(dataStimAvg_AlphaBeta{indCond,1}.individual(:,[26, 27, 35, 36],:),2)); % contralateral
    end
    cla; hold on;
    patches.timeVec = [0 3.040];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [.3 2]*10^-5;
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    condAvg = squeeze(nanmean(BLBeta,1));
    curData = squeeze(BLBeta(1,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(dataStimAvg_AlphaBeta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLBeta(2,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(dataStimAvg_AlphaBeta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(2,:),'linewidth', 2}, 'patchSaturation', .1);
    %xlim([-1000 200]); xlabel('Time (ms); resp-locked')
    curData = squeeze(BLBeta(3,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(dataStimAvg_AlphaBeta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .1);
%     line([0 0], get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2)
%     line([3040 3000], get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2)
    legend([l1.mainLine, l2.mainLine, l3.mainLine], {'1 target'; '2+ targets, convergence'; '2+ targets, no convergence'}, 'location', 'North')
    %legend('boxoff');
    ylabel({'Contralateral mu-beta';'(8-25 Hz) power (a.u.)'});
    xlabel('Time from stimulus onset (s)')
    title('Contralateral mu-beta power')
    % add stats
    curmask = double(eval(['stat_1_1.mask']));
    curmask(curmask==0)=NaN;
    plot(dataStimAvg_AlphaBeta{1,1}.time, curmask*.35*10^-5, 'LineWidth', 3, 'color', cBrew(1,:)); 
    curmask = double(eval(['stat_1_2.mask']));
    curmask(curmask==0)=NaN;
    plot(dataStimAvg_AlphaBeta{1,1}.time, curmask*.41*10^-5, 'LineWidth', 3, 'color', cBrew(2,:)); 
    ylim([[.3 1.75]*10^-5])

    subplot(2,1,2);
    BLBeta = [];
    for indCond = 1:3
        BLBeta(indCond,:,:) = squeeze(nanmean(dataStimAvg_AlphaBeta{indCond,1}.individual(:,[30, 31, 39, 40],:),2)); % ipsilateral
    end
    cla; hold on;
    patches.timeVec = [0 3.040];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [.25 2]*10^-5;
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    % between-subject error bars
    % new value = old value ? subject average + grand average
    condAvg = squeeze(nanmean(BLBeta,1));
    curData = squeeze(BLBeta(1,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(dataStimAvg_AlphaBeta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(1,:),'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLBeta(2,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(dataStimAvg_AlphaBeta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(2,:),'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(BLBeta(3,:,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(dataStimAvg_AlphaBeta{1,1}.time,nanmean(curData,1),standError, 'lineprops', {'color', cBrew(4,:),'linewidth', 2}, 'patchSaturation', .1);
    ylabel({'Ipsilateral mu-beta';'(8-25 Hz) power (a.u.)'});
    xlabel('Time from stimulus onset (s)')
    title('Ipsilateral mu-beta power')
    % add stats
%     curmask = double(eval(['stat_2.mask']));
%     curmask(curmask==0)=NaN;
%     plot(dataStimAvg_AlphaBeta{1,1}.time, curmask*.35*10^-5, 'LineWidth', 3, 'color', 'k');
    curmask = double(eval(['stat_2_1.mask']));
    curmask(curmask==0)=NaN;
    plot(dataStimAvg_AlphaBeta{1,1}.time, curmask*.35*10^-5, 'LineWidth', 3, 'color', cBrew(1,:)); 
    curmask = double(eval(['stat_2_2.mask']));
    curmask(curmask==0)=NaN;
    plot(dataStimAvg_AlphaBeta{1,1}.time, curmask*.41*10^-5, 'LineWidth', 3, 'color', cBrew(2,:));
    
    ylim([[.25 1.75]*10^-5])
    set(findall(gcf,'-property','FontSize'),'FontSize',16)

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/C_figures/L_motor_probe/';
    figureName = 'L2_beta_stimLocked';

    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
