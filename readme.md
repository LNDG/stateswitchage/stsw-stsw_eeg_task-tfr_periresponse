[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## Contralateral mu–beta

Decreases in contralateral mu-beta power provide a complementary, effector-specific signature of evidence integration28,83. We estimated mu-beta power using seven-cycle wavelets for the 8–25 Hz range with a step size of 50 ms. Spectral power was time-locked to probe presentation and response execution. We re-mapped channels to describe data recorded contra- and ipsilateral to the executed motor response in each trial, and averaged data from those channels to derive grand average mu-beta time courses. Individual average mu-beta time series were baseline-corrected using the −400 to −200 ms time window prior to probe onset, separately for each condition. For contralateral motor responses, remapped sites C3/5 and CP3/CP5 were selected based on the grand average topography for lateralized response executions (see inset in Supplementary Fig. 2a). As a proxy of evidence drift rate, mu-beta slopes were estimates via linear regression from -200 to −50 ms prior to response execution, while the average power −50 to 50 ms served as an indicator of decision thresholds (e.g., ref. 28).

## Function overview

**a01_calculate_beta_power**

- calculate beta power for individual subjects
- CSD transform
- 7-cycle wavelet (8 to 25 Hz, -1s to +3.5s peri-stimulus)
- resegment to probe on- and offset
- split into left and right response condition for laterality
- create multiple conditions (see script)
- average across trials

**a02a_beta_groups**

- create individual averages: average between 12 and 25 Hz
- group by age and load

**a02b_plot_beta**

- baseline-adjust individual averages: -400ms to -200ms pre-probe onset on grand average beta series (condition-specific): (signal-bl)/bl

Response-locked beta traces YA:
![image](figures/A_contralateralBeta_YA.png)

Response-locked beta traces OA:
![image](figures/A_contralateralBeta_OA.png)

Beta topography around response (-50:50 ms peri-response) YA:
![image](figures/B_contralateralBeta_topography_YA.png)

Beta topography around response (-50:50 ms peri-response) OA:
![image](figures/B_contralateralBeta_topography_OA.png)

Threshold estimates YA:
![image](figures/C_contralateralBeta_threshold_YA.png)

Threshold estimates OA:
![image](figures/C_contralateralBeta_threshold_OA.png)